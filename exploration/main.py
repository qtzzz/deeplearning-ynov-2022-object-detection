import tensorflow as tf

import numpy as np
import IPython.display as display

# Create a dictionary describing the features.
image_feature_description = {

    'image/height': tf.io.FixedLenFeature([], tf.int64),
    'image/width': tf.io.FixedLenFeature([], tf.int64),
    'image/filename': tf.io.FixedLenFeature([], tf.string),
    'image/source_id': tf.io.FixedLenFeature([], tf.string),
    'image/encoded': tf.io.FixedLenFeature([], tf.string),
    'image/format': tf.io.FixedLenFeature([], tf.string),
    'image/object/bbox/xmin': tf.io.FixedLenFeature([], tf.int64),
    'image/object/bbox/xmax': tf.io.FixedLenFeature([], tf.int64),
    'image/object/bbox/ymin': tf.io.FixedLenFeature([], tf.int64),
    'image/object/bbox/ymax': tf.io.FixedLenFeature([], tf.int64),
    'image/object/class/text': tf.io.FixedLenFeature([], tf.string),
    'image/object/class/label': tf.io.FixedLenFeature([], tf.string),
}

def _parse_image_function(example_proto):
  # Parse the input tf.train.Example proto using the dictionary above.
  return tf.io.parse_single_example(example_proto, image_feature_description)


train_image_dataset = tf.data.TFRecordDataset('data/train.tfrecord')
training_ds = train_image_dataset.map(_parse_image_function)
validation_image_dataset = tf.data.TFRecordDataset('data/validation.tfrecord')
validation_ds = validation_image_dataset.map(_parse_image_function)




#create the common input layer
input_shape = (300, 300, 3)
input_layer = tf.keras.layers.Input(input_shape)

#create the base layers
# base_layers = tf.keras.layers.experimental.preprocessing.Rescaling(1./255, name='bl_1')(input_layer)
base_layers = tf.keras.layers.Conv2D(16, 3, padding='same', activation='relu', name='bl_2')(input_layer)
base_layers = tf.keras.layers.MaxPooling2D(name='bl_3')(base_layers)
base_layers = tf.keras.layers.Conv2D(32, 3, padding='same', activation='relu', name='bl_4')(base_layers)
base_layers = tf.keras.layers.MaxPooling2D(name='bl_5')(base_layers)
base_layers = tf.keras.layers.Conv2D(64, 3, padding='same', activation='relu', name='bl_6')(base_layers)
base_layers = tf.keras.layers.MaxPooling2D(name='bl_7')(base_layers)
base_layers = tf.keras.layers.Flatten(name='bl_8')(base_layers)

#create the classifier branch
classifier_branch = tf.keras.layers.Dense(128, activation='relu', name='cl_1')(base_layers)
classifier_branch = tf.keras.layers.Dense(2, name='cl_head')(classifier_branch)  

locator_branch = tf.keras.layers.Dense(128, activation='relu', name='bb_1')(base_layers)
locator_branch = tf.keras.layers.Dense(64, activation='relu', name='bb_2')(locator_branch)
locator_branch = tf.keras.layers.Dense(32, activation='relu', name='bb_3')(locator_branch)
locator_branch = tf.keras.layers.Dense(4, activation='sigmoid', name='bb_head')(locator_branch)

model = tf.keras.Model(input_layer,outputs=[classifier_branch,locator_branch])
losses = {"cl_head":tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True), 
   "bb_head":tf.keras.losses.MSE}
model.compile(loss=losses, optimizer='Adam', metrics=['accuracy'])
history = model.fit(x=train_image_dataset, validation_data=(validation_image_dataset), batch_size=4, epochs=5, shuffle=True, verbose=1)